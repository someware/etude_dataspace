# Etude de la plateforme Copernicus Data Space

Septembre 2023

| | | |
|-----------------|:---------------:|:---------------:|
| ![Logo Connect by CNES](img/cnes.svg "Connect by CNES") |  ![Logo FPCUP](img/fpcup-logo.svg "Framework Partnership Agreement on Copernicus User Uptake") | ![Logo SOMEWARE](img/someware.png "SOMEWARE") |
| [https://www.connectbycnes.fr](https://www.connectbycnes.fr) | [https://www.copernicus-user-uptake.eu/](https://www.copernicus-user-uptake.eu/) | [https://www.someware.fr](https://www.someware.fr) |


## 1. Objet de l'étude

En 2020 et 2022, [Someware](https://www.someware.fr) a été mandaté par le [CNES](https://www.cnes.fr) pour conduire une analyse comparative des plateformes Copernicus DIAS. Ce travail a été [publié de façon ouverte en ligne](https://gitlab.com/idgeo_public/etude-dias) et présenté à plusieurs occasions.

Par ailleurs, Someware a participé à l’action DIAS Priming en 2022 afin d’assister quatre entreprises, sélectionnées par le CNES pour leurs cas d’usages divers, dans la sélection des plateformes DIAS appropriées à leurs besoins.

Alors que le financement des DIAS s’est achevé fin 2022 pour donner lieu à l’avènement d’une nouvelle plateforme ESA (sous le nom DAS pour “Data Access Service”, ou “Data Space”), il apparaît nécessaire d’étudier cette nouvelle offre et comment elle s’inscrit dans le paysage actuel des plateformes diffusant les données du programme Copernicus.

La première version de la plateforme [Data Space](https://dataspace.copernicus.eu/) est entrée en service en janvier 2023, avec un calendrier de montée en versions/fonctionnalités s'étirant jusqu'à la fin 2023.

Réalisée durant l'été 2023, et financée dans le cadre du programme européen FPCUP / Caroline Herschel, la présente étude est à la fois une étude théorique de l'offre de la plateforme Data Space, ainsi qu'une étude pratique intégrant quelques tests de la plateforme et l'analyse de cas d'usage concrets.
Elle vise à proposer un premier regard, aussi objectif que possible, sur la plateforme et sur comment celle-ci s'inscrit dans son écosystème.

## 2. Abstract

In 2020 and 2022, [Someware](https://www.someware.fr) was commissioned by [CNES](https://www.cnes.fr) to conduct a comparative analysis of the Copernicus DIAS platforms. This work has been [published openly online](https://gitlab.com/idgeo_public/etude-dias) and presented on several occasions.

In addition, Someware took part in the 2022 DIAS Priming action to assist four companies, selected by CNES for their various use cases, in selecting the DIAS platforms best suited to their needs.

With DIAS funding coming to an end at the end of 2022, leading to the advent of a new ESA platform (under the name DAS for 'Data Access Service', or 'Data Space'), it seems necessary to study this new offering and how it fits into the current landscape of spatial data platforms.

The first version of the [Data Space platform](https://dataspace.copernicus.eu/) went live in January 2023, with a timetable for upgrading versions/features stretching to the end of 2023.

Carried out during the summer of 2023, and financed by the european program FPCUP / Caroline Herschel, this study is both a theoretical study of the Data Space platform offer and a practical study incorporating a number of tests of the platform and the analysis of concrete use cases.
Its aim is to provide as objective a first look as possible at the platform and how it fits into its ecosystem.

## 3. Présentation de Someware

Créée en 2019, [Someware](https://www.someware.fr) est une société de conseil et développement logiciel au service de l'information géographique.

La société est spécialisée dans la conception d’applications métier (web / mobile / desktop) à forte composante geospatiale, et conseille de nombreux organismes dans leurs besoins d'exploitation de données géographiques.

En particulier, Someware développe une gamme d'outils dans le domaine des mobilités actives, avec le calculateur d'itinéraires accessibles Handimap et différents outils d'audit de la marchabilité/accessibilité des villes.

Someware est reconnue pour sa connaissance de l'écosystème Open Data, et exploite fréquemment des données spatiales dans le cadre de ses activités.

La société a notamment réalisé une étude des plateformes Copernicus DIAS, mise à jour à deux reprises, et a développé les sites web [Tropisco.org](https://www.vietsco.org), [Vietsco.org](https://www.vietsco.org) et [Copernicus Regional](http://copernicus-regional.irispace.fr/).


## 4.​ Méthodologie

L'étude se veut d'abord théorique, avec une première phase bibliographique visant à recenser les caractéristiques de la plateforme.

Cette première phase théorique a ensuite été complétée par un entretien, par écrit, avec un des représentants de la plateforme (société Sinergise, rachetée par Planet en août 2023).

Enfin, l'étude a porté sur quelques tests de la plateforme, de façon à analyser sa praticabilité, ainsi que sur l'analyse de plusieurs cas d'usage concrets issus de 4 sociétés.


## ​5.​ Généralités

Pour comprendre ce qu'apporte la plateforme Data Space, il est important de connaître le contexte dans lequel s'inscrit.

### 5.1.​ Le programme Copernicus

Copernicus est le nom d'un programme européen de surveillance de la Terre, auparavant nommé GMES (Global Monitoring for Environment and Security). Il s'agit d'une initiative conjointe de l'ESA (Agence spatiale européenne) et, au travers de l'Agence européenne pour l'environnement (AEE), de l'Union européenne, qui vise à doter l'Europe d'une capacité opérationnelle et autonome d'observation de la Terre en tant que services d'intérêt général européen, à accès libre, plein et entier.

Plus concrètement, Copernicus vise à rationaliser l'utilisation de données relatives à l'environnement et à la sécurité issues de sources multiples, afin de disposer d'informations et de services fiables chaque fois que cela est nécessaire. En d'autres termes, Copernicus permet de rassembler l'ensemble des données obtenues à partir de satellites environnementaux et d'instruments de mesure sur site, afin de produire une vue globale et complète de l'état de notre planète.

Les domaines couverts par Copernicus sont vastes, et comprennent (liste non exhaustive) :

- l'évolution des teneurs atmosphériques en aérosols et gaz à effet de serre
- le suivi de couche d'ozone, et le taux d'ultraviolet
- la climatologie, prévisions de l'état de la mer, sécurité maritime, suivi du trafic maritime et de certaines pollutions marines (marées noires, dégazages...)
- la mesure, le contrôle et la gestion du développement urbain (urbanisation, périurbanisation...).
- la montée du niveau des océans
- l'alerte aux aléas climatiques et catastrophes naturelles (tempêtes, inondations, sécheresse, fortes pluies, tremblements de terre, tsunamis, connaissance et suivi des inondations et des feux de forêt pour leur meilleure gestion…)
- la surveillance de l'environnement, de l'agriculture, des forêts, de la déforestation et de leurs conséquences
- l'anticipation, alerte et gestion de catastrophes humanitaires (déplacements de population, migration humaine, camps de réfugiés, séquelles des guerres…)
- la sécurité civile, organisation des secours
- la sécurité et surveillance des frontières
- la lutte contre les trafics (par exemple de bois, de drogue…) et contre la piraterie en mer,
- la surveillance de zones (marines notamment) isolées ou provisoirement isolées.
- la disponibilité ou surexploitation de ressources naturelles.

#### Capteurs

Pour mener à bien ses missions, Copernicus s'appuie en premier lieu sur la technologie spatiale et les moyens au sol des pays membres du programme. De nombreuses missions contribuent au programme :

- Satellites optiques haute résolution : Pléiades, Deimos-2, RapidEye, SPOT HRS,
- Satellites optiques basse résolution : SPOT VGT, PROBA-V,
- Satellites radar : COSMO-Skymed, TerraSAR-X, Tandem-X, RadarSat,
- Satellites radar altimétriques : CryoSat, Jason
- Satellites météorologiques: MetOp, MeteoSat 2ème génération


Le CNES (Centre National d'Etudes Spatiales) met ainsi à disposition de Copernicus plusieurs satellites comme Spot, Jason et Pleiades. D'autres capteurs non spatiaux, dits in-situ, sont aussi mis à sa disposition par les États (avec une coordination au niveau européen), tels que des instruments aéroportés, ballons, bouées, stations de mesure, sismographes…

Par ailleurs, Copernicus compte sur les satellites Sentinels, spécifiquement développés dans le cadre du programme.

La série de satellites Sentinel est dédiée à l'observation de la Terre, des océans et de l'atmosphère. Les missions Sentinel ont les objectifs suivants :

- **Sentinel-1** : fourniture d'imagerie radar tout-temps, jour et nuit, à des fins d'observation du sol et des océans. Sentinel-1A a été lancé en 2014 et Sentinel-1B en 2016 pour atteindre l'objectif d'une fréquence de prise de vues de 6 jours pour l'ensemble de la planète (résolution 9-40m).
 Le volume de données produites est de l'ordre de 4-7 PB/année.

- **Sentinel-2** : fourniture d'imagerie optique haute résolution pour l'observation des sols (utilisation des sols, végétation, zones côtières, fleuves, etc.). Sentinel-2 est également utile pour la mise en place de services de traitement de l'urgence. Le premier satellite Sentinel-2A a été lancé en 2015, et le satellite Sentinel-2B a été lancé en 2017 pour atteindre l'objectif d'une fréquence de prise de vues de 5 jours pour l'ensemble de la planète (résolution 10-60m - 13 bandes spectrales).
 Le volume de données produites est de l'ordre de 3-4 Petabytes/année.

- **Sentinel-3** : surveillance mondiale des océans et des sols. Deux satellites Sentinel-3 ont été lancés en 2016 et 2018, pour une fréquence de prise de vues inférieure à 2 jours (résolution 300-1200m).

Le volume de données produites est de l'ordre de 3-4 Petabytes/année.

- **Sentinel-4** : observation de la composition chimique de l'atmosphère à haute résolution temporelle et spatiale. Le satellite doit être lancé en 2023.

- **Sentinel-5** : observation de la composition chimique de l'atmosphère. Le satellite Sentinel-5p a été lancé en 2017 comme solution d'attente au satellite Sentinel-5, plus complet, qui sera lancé en 2021. Il a pour objectif de mesurer la quantité de certains gaz et aérosols présents dans l'atmosphère terrestre (quantité et répartition) ayant un impact sur la qualité de l'air et le climat.
 Le volume de données produites par Sentinel-5p est de l'ordre de 0.5 Petabytes/année.

- **Sentinel-6** : étude quasi temps réel de la surface des océans (vagues, vents de surface, hauteur de la mer)à destination des prévisions de météorologie marine à court terme, des prévisions saisonnières (El Niño, ...) et des recherches sur le changement climatique. Le premier satellite Sentinel 6A a été lancé en novembre 2021, et Sentinel 6B sera lancé en 2025.

#### Core Services

Les observations produites par l'ensemble des satellites et capteurs à disposition du programme Copernicus sont diffusées sous la forme de services (&quot;Core Services&quot;), regroupant les informations en 6 thématiques.

Thématiques par milieu :

- Atmosphère ([CAMS](https://www.copernicus.eu/fr/services/atmosphere))
- Marin ([CMEMS](https://www.copernicus.eu/fr/services/marin))
- Terrestre ([CLMS](https://www.copernicus.eu/fr/services/terrestre))

Thématiques par thème:

- Changement climatique ([C3S](https://www.copernicus.eu/fr/services/changement-climatique))
- Sécurité([CSS](https://www.copernicus.eu/fr/services/securite))
- Urgences ([CEMS](https://www.copernicus.eu/fr/services/urgence))

L'objectif de chaque _Core Service_ est de fournir de la donnée à valeur ajoutée, à partir de l'analyse et le traitement des données brutes issues de capteurs.

Des données s'étalant sur des années, voire des dizaines d'années, sont ainsi rendues consultables et comparables, facilitant la détection de tendances, motifs, et anomalies, le calcul de statistiques...


### 5.2.​ Les plateformes DIAS

Le projet DIAS a constitué la première étape d'un chantier qui amène à la création de la plateforme Data Space.

#### **Historique**

Le projet Copernicus DIAS (Data and Information Access Services) est une initiative de la Commission Européenne, pilotée par l'Agence Spatiale Européenne (ESA), dont l'objectif a été de faciliter l'accès aux données du programme Copernicus, ainsi qu'à des solutions de calcul dans le cloud pour exploiter ces données.

Lancé en 2017, et prémice du projet Data Space, la Commission Européenne a ainsi souhaité avec le projet DIAS ouvrir l'usage des données du programme Copernicus à un panel nouveau d'entreprises ou organismes, de façon à créer de nouveaux marchés ou dynamiser des domaines existants.

Par ailleurs, la Commission Européenne a souhaité proposer une solution compétitive et souveraine pour l'accès aux données du programme Copernicus, à même de rivaliser avec les plateformes américaines qui se développaient 

Un appel d'offre est lancé en 2017 pour sélectionner plusieurs consortiums à même de mettre au point une plateforme de stockage et diffusion des données du programme Copernicus, ainsi qu'une offre permettant à de futurs utilisateurs de réaliser des calculs sans avoir à investir dans des solutions lourdes.

D'autre part, les consortiums s'engagent à donner un accès illimité, gratuit et total aux données du programme Copernicus (Core Services compris) à tout type d'utilisateurs.

Selon l'ESA, le succès des DIAS va être fonction de l'attractivité de leurs services et de leur capacité à proposer des solutions offrant en standard plus de qualité / valeur que celles qui pourraient être développées en interne par leur client. D'autre part, le succès va dépendre de la capacité de chaque DIAS à attirer une communauté de fournisseurs de services basés sur chaque plateforme, de façon à créer un écosystème de services.

#### **Cinq plateformes DIAS**

Cinq consortiums sont retenus en décembre 2017 pour mettre en oeuvre, de façon concurrente, le cahier des charges :

- **[CREODIAS](https://creodias.eu/)** : consortium représenté principalement par CloudFerro (Cloud Provider basé en Pologne) et Sinergise (société slovène spécialisée dans les solutions logicielles s'appuyant sur de l'imagerie spatiale)

- **[MUNDI WEB SERVICES](https://mundiwebservices.com/)** : consortium représenté principalement par Atos, T-Systems (Open Telekom Cloud), Thales Alenia Space et Sinergise (société slovène spécialisée dans les solutions logicielles s'appuyant sur de l'imagerie spatiale)

- **[ONDA](https://www.onda-dias.eu)** : consortium représenté principalement par Serco Italia, OVH, Gael Systems et Sinergise (société slovène spécialisée dans les solutions logicielles s'appuyant sur de l'imagerie spatiale)

- **[SOBLOO](https://sobloo.eu/)** : consortium représenté principalement par Airbus, Capgemini et Orange Business Services

- **[WEKEO](https://www.wekeo.eu/)** : consortium représenté principalement par Mercator Océan, EUMETSAT (organisation européenne pour l'exploitation de satellites météorologiques) et ECMWF (centre européen pour les prévisions météorologiques à moyen terme)

Les cinq consortiums délivrent la première version de leur solution aux trimestres Q1/Q2 de 2018, et sont engagés contractuellement avec l'ESA jusqu'en décembre 2020, puis prolongés jusqu'à la fin 2022.

#### **Bilan des DIAS**

Les plateformes DIAS ont été une expérience de la Commission Européenne pour développer une offre souveraine et compétitive d'accès aux données du programme Copernicus.

Auparavant entre les mains d'organismes publics, du monde académique ou grands acteur américains du Cloud, la diffusion des données Copernicus a pu être proposée par des organismes privés européens dont les services et priorités sont orientés client, avec ce qui en découle : une compétition pour proposer les meilleurs services (fonctionnalités, contenu, performances, fiabilité…) aux meilleurs coûts. A condition qu'une réelle concurrence s'établisse dans le temps, cela devrait être bénéfique pour tous les utilisateurs.

L'offre proposée par les DIAS a surtout eu pour plus-value, par rapport à l'existant, de permettre un accès très performant aux données depuis le cloud de chaque plateforme (ce que le secteur recherche/académique ne proposait pas), et des mécanismes basés sur des protocoles ouverts (OpenSearch, OGC) pour découvrir les données.

En revanche, l'offre en termes de calcul est restée très pauvre : difficile par nature à définir car devant répondre à des besoins très variés, les plateformes (et l'ESA) n'ont pas réussi à apporter une réelle plus-value dans le domaine.
En résumé, l'offre cloud des plateformes n'a eu pour ainsi dire d'intéressant, par rapport à d'autres offres cloud, que la possibilité d'accéder de façon très performante aux données.

Sans doute trop ambitieuse au regard de son budget, et peut-être déviée de son intention par du lobbying (au départ, seules deux plateformes DIAS devaient être financées), l'initiative DIAS n'a au final pas réussi à trouver un modèle économique viable et à installer une offre européenne à même de concurrencer les géants du Cloud américains.

De ce constat est apparu la nécessité, pour la Commission Européenne, de rebondir en finançant la mise au point et le fonctionnement d'une seule plateforme, Data Space, qui fait l'objet de la présente étude.


### 5.3.​ L'écosystème de plateformes de données spatiales

Les plateformes de données spatiales sont des portails web proposant a minima un large catalogue de produits satellitaires gratuits issus de programmes tels que Copernicus ou Landsat, ou payants tels Pléiades. Cette offre est généralement complétée par un ensemble d'outils facilitant l'accès à ces données ou la mise en oeuvre de calculs.

Ces portails se développent fortement depuis une dizaine d'années, sous l'impulsion notamment des géants du cloud, avec des ambitions et des moyens différents :

- public cible : entreprises, communauté scientifique, communauté 'métier' spécifique
- services, performances et accompagnement adaptés à des usages spécifiques

Parmi les nombreuses plateformes existantes, les suivantes sont particulièrement actives en Europe :

**[Open Access Hub / SciHub](https://scihub.copernicus.eu/)**

L'ESA propose en particulier la plateforme Copernicus Open Access Hub (aussi appelée SciHub pour Sentinels Scientific Data Hub).

Ce portail permet d'accéder aux données Sentinel via deux types d'interfaces différents : une interface graphique interactive et des API. Les deux interfaces permettent aux utilisateurs de définir différents paramètres (zone géographique, heure, type de produit, etc.) pour affiner leur recherche dans les archives de produits. Le portail donne également accès aux données produites par les missions Sentinel dès qu'elles seront disponibles.

**[Sentinel Hub](https://www.sentinel-hub.com/)**

Proposée par la société Sinergise, aussi membre du projet Data Space, cette plateforme propose un catalogue et des outils pour accéder ou traiter des données de différents programmes (Copernicus, Landsat...)

**[Amazon Web Services (AWS)](https://aws.amazon.com/fr/earth/)**

Proposée par Amazon via son offre AWS, il s'agit d'une offre en ligne de données satellitaires, opérée par différentes sociétés (dont Sinergise) en tirant parti des fonctionnalités de l'offre cloud d'Amazon.

**[Google Earth Engine](https://earthengine.google.com/)**

Plateforme basée sur le cloud de Google, proposant un catalogue de données spatiale, ainsi que des outils permettant de développer des applications / algorithmes de traitement.

**[Microsoft Planetary Computer](https://planetarycomputer.microsoft.com/)**

Plateforme basée sur Azure, le cloud de Microsoft, proposant un large catalogue de données spatiales ainsi que des outils de traitement.


## 6. Copernicus Data Space

### 6.1. Enjeux et ambition

Loin d'être simplement un portail permettant d'accéder aux données spatiales produites par l'Europe, [Copernicus Data Space](https://dataspace.copernicus.eu/) se veut un écosystème combinant des données et des services pour exploiter ces données.

L'ambition de la plateforme est de devenir incontournable à échelle mondiale pour accéder à des données du programme Copernicus, mais aussi à des données Landsat ou commerciales (Airbus, Planet, Maxar, AxelSpace, SIIS).
Ces données sont le plus souvent à l'état brut, mais aussi traitées.

Par ailleurs, la plateforme propose des services d'analyse/traitement de données, avec des APIs de calcul éprouvée (Sentinel Hub, openEO) et une offre commerciale cloud sous le nom CREODIAS (rebranding du DIAS "CREODIAS" pour proposer une offre basée sur les clouds de Open Telekom Cloud (T-Systems) et Cloudferro.

Avec une ambition très similaire aux DIAS, un financement solide, et opérée par un consortium intégrant plusieurs acteurs des DIAS (T-Systems, CloudFerro, Sinergise, VITO, DLR, ACRI-ST et RHEA), Data Space se veut donc un "Super DIAS", avec une multitude d'APIs et services pour accéder aux données ou les traiter.

Il apparaît donc plus que nécessaire de donner de la visibilité sur l'offre très large de Data Space.


### 6.2. Fonctionnalités et services

#### ​6.2.1​ Données

**Catalogue de données**

L'ambition de Data Space est de fournir le plus vaste catalogue de données Copernicus disponibles en téléchargement direct en ligne.

Ce catalogue est décrit à l'adresse suivante: [https://documentation.dataspace.copernicus.eu/Data.html](https://documentation.dataspace.copernicus.eu/Data.html)

Le catalogue comprend à la fois des données gratuites telles que les données Sentinel, Copernicus Core Services, Landsat ou quelques DEM. Leur accès est possible gratuitement en respectant les quotas gratuits de la plateforme.

Il comprend aussi des données commerciales ou proposées par un organisme commercial, via la plateforme Creodias ou Sentinel Hub par exemple. Dans ce cas, l'accès est assujetti à des coûts d'acquisition des données et/ou des coûts d'exploitation des services de chaque plateforme.

Pour [plus d'informations sur les coûts et quotas](#64-tarification-et-quotas).

Pour comprendre comment accéder à chaque type de produits, quelques notions sont indispensables :
- **Archive Status** : informations sur le format des données. "Packed" si les données sont zippées, "Unpacked" sinon. Certaines données peuvent aussi être disponibles sous la forme de COG, ou avec des traitements spécifiques au produit.
- **Access Type** : IAD (Immediately Available Data) ou DAD (Deferred Available Data). Ces acronymes signifient que les données sont disponibles soit immédiatement (IAD), sans temps d'attente("à chaud"). Soit en décalé ("DAD), avec un temps d'attente, autrement dit ces données sont archivées ("à froid") et doivent donc être extraites par la plateforme pour être téléchargeables depuis l'application [Data Workspace](#​622​-applications).
- **Spatial Extent** : il s'agit de la couverture spatiale proposée par la plateforme (World, Europe)
- **Temporal Extent** : il s'agit de la couverture temporelle proposée par la plateforme. Par exemple, pour le produit Sentinel 3 
- **Available in Ecosystem from** : date de mise en ligne du produit sur la plateforme Data Space

Autrement dit, même si l'ambition de la plateforme est de proposer un très large catalogue de données, il reste indispensable d'étudier la disponibilité spatiale/temporelle des produits, et leur disponibilité à chaud ou froid.


**Localisation des données hébergées**

Comme le consortium derrière la plateforme se compose de deux Cloud Providers (Open Telekom Cloud, de T-Systems, et Cloudferro) avec des datacenters en Allemagne et Pologne, il est important de souligner que les données sont hébergées dans ces deux pays, sans réplication sur des clouds sur chaque continent comme peuvent parfois le proposer les grands Cloud Providers.

Pour des utilisateurs basés hors d'Europe, les temps de téléchargement des données depuis Data Space sont donc plus longs, ce qui peut s'avérer réellement problématique pour [certains cas d'usage](#​66​-etude-de-cas-dusage).


**Formats de données**

Les API d'accès aux données de la plateforme proposent par défaut les formats proposés par les différents programmes d'observation de la terre, autrement dit le format SAFE pour les données Sentinel (avec de l'imagerie au format JPEG2000).

Néanmoins, certaines données peuvent parfois être proposées au format COG (Cloud Optimized GeoTIFF).

Par ailleurs, certaines APIs spécifiques peuvent proposer d'autres formats, comme l'API Sentinel Hub WMS qui propose des formats web (PNG, JPEG, TIFF, Shapefile, GeoJSON).

**Délai de disponibilité des données**

L'intégration des nouveaux produits dans Copernicus Data Space se fait à partir de SciHub puis, à partir de 2024, sera réalisée à partir d'autres portails propres à chaque type de produit.

Les temps d'intégration de chaque produit dans Copernicus Data Space varient de quelques heures à quelques jours selon le type de produit.

Ces temps ne sont pas aujourd'hui communiqués officiellement. Ils sont en moyenne de 5 heures.


**Données calculées**

La plateforme ne propose pas de données calculées spécifiques, hormis celles des Copernicus Core Services ou des données telles que Sentinel 2 L2A.

Des produits type NDVI avec une résolution de 10 mètres ne sont donc pas disponibles en téléchargement.

Néanmoins, les APIs de calcul de la plateforme permettent de les calculer.


**API de catalogage et téléchargement de données**

Plusieurs types d'APIS sont proposées pour rechercher et télécharger des données.

Certaines APIs permettent de requêter le catalogue de la plateforme, et une API est spécifique au catalogue de la plateforme Sentinel Hub (opérée par Sinergise et VITO).

Autrement dit, lors de la recherche de certaines données, il est important d'étudier si elles se trouvent sur la plateforme, avec des conditions d'accès garanties par l'ESA, ou sur Sentinel Hub avec des conditions potentiellement différentes, négociées avec les sociétés Sinergise et VITO.

Les APIs de catalogage sont les suivantes:

- **API OData** : basée sur le protocole OData de Microsoft, cette API permet de requêter l'ensemble des produits (sauf certaines collections uniquement disponibles via Sentinel Hub), qu'ils soient disponible à chaud/froid, avec différents filtres possibles. Elle permet de télécharger les produits soit zippés, soit une partie/sélection d'un produit seulement.

- **API STAC** : encore en beta lors de la réalisation de l'étude, cette API conforme au standard STAC permet de rechercher les produits disponibles à chaud

- **API OpenSearch** : basée sur le protocole standard OGC OpenSearch, cette API propose un modèle de données basé sur GeoJSON et permet de filtrer selon quelques critères.

- **API Sentinel Hub Catalogue**: aussi basée sur le standard STAC, cette API permet de rechercher les données disponibles dans la plateforme Sentinel Hub.

Pour le téléchargement de données, la principale API est la suivante:

- **API S3**: cette API est dédiée au téléchargement de la majorité des données proposées par la plateforme. Autrement dit, il est nécessaire d'utiliser une des API ci-dessus pour rechercher le catalogue et identifier les données à télécharger avec leur adresse S3. Puis celles-ci peuvent être téléchargées via l'API S3. Dans le cas de données au format COG, l'API S3 (protocole HTTP) permet de télécharger seulement une partie des données (sélection sur une boîte englobante), réduisant ainsi les temps de téléchargement.

Pour accéder aux données via des protocoles standards OGC, comme WMS, WMTS, WFS, WCS, la seule solution est l'**API Sentinel Hub**, ne faisant pas directement partie de Data Space (même si considérée comme intégrée à l'écosystème Data Space). Cette API est à la fois une API de téléchargement et une API permettant de réaliser des calculs simples sur les données (voir la section ci-dessous).

#### ​6.2.2​ Services de calcul et offre cloud

**API de calcul**

La plateforme/écosystème Data Space propose deux APIs de calcul :

- **APIs Sentinel Hub** : APIs HTTP permettant de calculer des statistiques, ou d'appliquer des fonctions de calcul simples (scripts Javascript) sur chaque pixel d'une image en sélectionnant les bandes souhaitées. 

- **API openEO** : cette API permet d'écrire des traitements simples ou complexes sous la forme de workflows dans des langages tels que Python, R ou Javascript. Ces workflows sont ensuite exécutés par la plateforme, et permettent d'exporter les résultats dans des formats classiques dans le monde de la Data Science (GeoTIFF, GeoJSON, CSV, netCDF)... 
L'API openEO peut être paramétrée pour déclencher un calcul dès la parution d'un nouveau jeu de données. En plus de l'API, openEO propose un éditeur web ainsi qu'une base d'algorithmes/scripts publics.

Parmi les services de calcul proposés par la plateforme, le service de **Production à la demande ('On-Demand Processing' ou ODP)** est une API employée par la plateforme pour produire des niveaux 1, 2 ou 3 à partir de niveaux supérieurs. Cette API sera proposée aux utilisateurs en octobre 2023 pour leur permettre de spécifier les réglages fins de chaque service de calcul, de façon à produire des niveaux 1/2/3 répondant à leurs besoins.

**Offre cloud**

La plateforme propose aussi une **offre cloud**, sous le nom **CREODIAS** (rebranding de l'ancien DIAS du même nom), qui correspond en premier lieu aux offres combinées d'Open Telekom Cloud (T-Systems) et Cloudferro. Autrement dit, chaque société propose ses services cloud, similaires à d'autres opérateurs cloud tels qu'OVH, Amazon, Google... avec des services standards (machines virtuelles, S3/Object Storage...), quelques services propres et sa tarification.

Pour les utilisateurs, l'intérêt de cette offre cloud est de proposer :
- aucune limite/quota pour les téléchargements réalisés depuis une VM d'un des clouds
- la possibilité d'exploiter des services tels qu'openEO, Sentinel Hub, Sen4CAP ainsi que des services de génération de produits préconfigurés (sous le nom PGaaS)
- des crédits et une offre packagée, plutôt que des coûts d'utilisation fonction de l'usage et de l'évolution des prix, permettant à des organismes publics (peu flexibles en termes de budget) de s'appuyer plus facilement sur la plateforme


#### ​6.2.2​ Applications

La plateforme propose trois types d'applications web pour explorer ses données et tester ses services:

- **Browser** : catalogue web permettant de rechercher des données à chaud ou froid, et de les consulter dans une interface de WebMapping. Les données peuvent ensuite être ajoutées au Data Workspace, qui fait office de “Favoris”.

- **Data Workspace** : application web permettant de consulter son catalogue de données online/offline (IAD / DAD) et de commander des données calculées, qui peuvent ensuite être téléchargées.

- **JupyterHub** : environnement interactif très populaire dans l'univers de la Data Science, avec les Jupyter Notebooks. La plateforme permet à chaque utilisateur de disposer d'un container (équivalent d'une machine virtuelle, avec des limites en termes de CPU/RAM) pour accéder aux données et développer des traitements.

Enfin, comme toute plateforme de ce type, Data Space proposera une **Market Place** pour mettre en avant les applications s'appuyant sur ses données et services, de façon à montrer à quel point elle peut être considérée comme fiable.

#### 6.2.3 Autres services

**Bring your own data**

Prévu pour juillet 2023, ce service doit permettre aux utilisateurs d'intégrer leurs propres données dans les services de catalogage, téléchargement et calcul de la plateforme.

**API de traçabilité**

Cette API permet à un utilisateur d'obtenir des informations sur l'ensemble du cycle de vie d'un produit, comprendre les traitements qui ont été appliqués et données source employées.


### 6.3. Test de la plateforme

Les tests de la plateforme ont porté exclusivement sur l'utilisation du catalogue (via l'application Browser et quelques APIs) et le téléchargement de données.

Semblant assez simple de prime abord, l'utilisation de la plateforme s'est avérée plus complexe en pratique, avec une documentation succincte et l'absence de tutoriels.

Voici ci-dessous les résultats de quelques tests :

**Test de l'application Browser**

Se présentant comme bon nombre d'applications de ce type, le browser permet de rechercher des jeux de données à partir de dates, couverture nuage... puis de les consulter avec un rendu graphique sur une interface de web mapping.

A la date du premier test (mi-août 2023), il n'a pas été possible de rechercher des jeux de données car l'application indiquait que le compte créé était invalide ou expiré. La création d'un nouveau compte n'a pas résolu le problème.
Un email a été envoyé au support technique, qui n'a pas identifié la cause du problème.

Lors du second test, le lendemain, l'application fonctionnait.

Le browser nécessite un temps de prise en main pour comprendre les paramètres de filtrage, les différents types de réglage possible. Le browser est directement branché sur les APIs Sentinel Hub, et est en fait directement le visualiseur de SentinelHub, intégré à Data Space.
Il permet la consultation et le paramétrage de couches de données calculées à la volée (type NDVI).

Dans son onglet Visualize, il permet de consulter par défaut des mosaïques à différentes dates pour différents produites.
Dans son onglet Search, il permet de rechercher des produits, de les visualiser et les télécharger.

Après un petit temps de prise en main, l'outil s'avère très agréable et clair d'utilisation.


**Test de l'API OData et du téléchargement**

L'API OData est simple à comprendre et est la mieux documentée. 

Les exemples ont permis de récupérer un 'access token' à partir du login/password du compte utilisateur, puis de rechercher et télécharger des données.

Dans le répertoire 'scripts' de l'étude, vous trouverez un exemple permettant d'obtenir un token et télécharger un produit issu de l'API ODAta.

**Test de l'API STAC**

L'API STAC est décrite sur le site de Data Space comme en version beta, et pas encore compatible avec tous les clients STAC du marché.

Un test avec le plugin STAC API Browser de QGIS (recommandé sur la doc officielle de la spécification STAC) a confirmé la documentation, car celui-ci n'a pas réussi à dialoguer avec l'adresse du catalogue STAC.

De même, l'utilisation d'une bibliothèque Python pour requêter l'API n'a pas complètement fonctionné (pas possible de rechercher des éléments dans le catalogue).
Le script utilisé est disponible dans le répertoire 'scripts' de l'étude.

Néanmoins un parcours manuel des adresses de l'API a permis de rechercher quelques données, qu'il a été possible de télécharger via un script Python et un 'access token'.

Dans le répertoire 'scripts' de l'étude, vous trouverez un exemple permettant d'obtenir un token et télécharger un produit issu de l'API ODAta.

**Conclusion des tests**

L'utilisation de la plateforme a d'emblée buté sur quelques problèmes :
- le non-fonctionnement (lors du premier test) du Browser, pourtant le premier outil employé par les utilisateurs d'ordinaire
- l'API STAC peu ou pas opérationnelle

Néanmoins l'API OData a parfaitement fonctionné, tout comme le téléchargement de données.

Le browser s'est avéré très pratique et ergonomique à partir du moment où il s'est mis à fonctionner.

Il est clair, à la date du test (août 2023), que certains des services de la plateforme sont encore peu utilisés et pas encore très robustes.

Le manque de tutoriels, et de documentation pratique (par exemple pour l'API STAC), est révélateur d'une plateforme encore en plein développement, avec peu de temps consacré à la documentation puisque les services ne sont pas encore pleinement opérationnels.

### 6.4. Tarification et quotas

La philosophie générale pour l'utilisation de la plateforme Data Space est de proposer des données et services gratuits, dans la limite du respect de certains quotas.

Le catalogue comprend à la fois des données gratuites telles que les données Sentinel, Copernicus Core Services, Landsat ou quelques DEM. Leur accès est possible gratuitement en respectant les quotas gratuits de la plateforme.

Il comprend aussi des données commerciales ou proposées par un organisme commercial, via la plateforme Creodias ou Sentinel Hub par exemple. Dans ce cas, l'accès est assujetti à des coûts d'acquisition des données et/ou des coûts d'exploitation des services de chaque plateforme.

Plusieurs types d'utilisateurs sont gérés par la plateforme avec, pour chaque type, des quotas différents:

- **Utilisateurs généraux de Copernicus** : ce sont les utilisateurs enregistrés sur la base d'un auto-enregistrement et n'entrant pas dans la typologie des utilisateurs ci-dessous. Il n'existe aucune restriction pour s'enregistrer et accéder à des données, dans la limite de certains quotas.
- **Utilisateurs Copernicus Services** : ce sont des utilisateurs issus d'institutions et organes de l'Union européenne, autorités publiques nationales ou régionales de l'UE ou d'un pays tiers
- **Partenaires du segment terrestre collaboratif de Copernicus** : cette typologie d'utilisateurs comprend tous les États participant au programme Copernicus, après la signature d'un accord avec l'ESA et la Commission européenne.
- **Partenaires internationaux de Copernicus** : cette typologie d'utilisateurs est ouverte aux partenaires internationaux, après signature d'un accord de coopération avec la Commission européenne et d'un accord d'exploitation avec l'ESA.


Pour les utilisateurs généraux, par exemple, les quotas sont les suivants :

|Application / API|Quotas / Prix|
|-----------------|:---------------:|
|**Copernicus Browser**|Recherche illimitée, consultation/téléchargement/export assujettis aux quotas de l'API Sentinel Hub|
|**JupyterHub**|Chaque utilisateur dispose d'un container avec 2 vCPU et 8 Go de RAM|
|**APIs de catalogage**|Aucune limite, hormis quelques sécurités sur le nombre max de requêtes/minute pour éviter une saturation des services|
|**APIs de téléchargement**|Aucune limite si les téléchargements sont réalisés depuis l'un des clouds de la plateforme (nécessite la location d'a minima une machine virtuelle).<br>  Si les téléchargements sont réalisés depuis l'extérieur, il existe des limites, notamment sur la bande passante (max 20 Mo/s), sur le nombre de connexions en parallèle (4) et le total téléchargé / mois (6 To).<br>  Pour l'accès aux COG via HTTP, la limite est de 50K requêtes/mois.|
|**APIs Sentinel Hub**|Pour calculer des données, les coûts se basent sur la notion d'['unité de calcul'](https://docs.sentinel-hub.com/api/latest/api/overview/processing-unit/), qui correspond à une image 3 bandes de 512x512 pixels et 16 bits maximum. <br> Pour accéder à des produits calculés tels que Sentinel 2 NDVI ou True Color, des coûts au km² traité sont proposés, variant entre 0.00000004 et 0.00025 €/km². Les quotas d'accès gratuits sont de 10K requêtes/mois, 300 requêtes/minute, 10000 unités de calcul/mois et 300 unités de calcul/minute.|
|**API openEO**|Chaque utilisateur se voit attribuer 1000 crédits à la création de son compte|
|**Data Workspace / Production à la demande**|25 produits calculés par mois, 2 en parallèle|

Autrement dit, la plateforme propose une offre gratuite ("Free tier") permettant d'évaluer son offre et de tester ses APIs.
Pour certains utilisateurs, cette offre gratuite peut être suffisante. 

A partir d'un usage dépassant les quotas de la plateforme, il n'existe à ce jour pas de prix publics pour l'usage de chaque type de services (hormis pour les services commerciaux tels que Creodias ou Sentinel Hub). Par exemple, pour télécharger des volumes de données de plus de 6 To / mois, ou avec une bande passante de plus de 20 Mo/s, via l'API S3, aucun prix public n'est annoncé.

Dans ce cas, il est nécessaire de contacter les représentants de la plateforme.

Pour d'autres types d'utilisateurs, en particulier pour la communauté scientifique, il est nécessaire de se référer aux documentations en ligne de la plateforme pour connaître les quotas en vigueur.

En particulier, via cette documentation de début 2023 : [https://documentation.dataspace.copernicus.eu/_docs/CDSE-SDE-TSY_Service%20Description%20and%20Evolution_1.1.pdf](https://documentation.dataspace.copernicus.eu/_docs/CDSE-SDE-TSY_Service%20Description%20and%20Evolution_1.1.pdf)


### ​6.5. Comparatif avec d'autres plateformes

Qu'est-ce que Data Space propose de plus ou de moins que les autres plateformes, dont certaines sont aujourd'hui très utilisées ? Est-ce que Data Space est plus ou moins cher, plus ou moins efficace ?

Il est difficile d'apporter une réponse simple à ces questions sans étudier des cas d'usage spécifiques.

La présente étude n'a pas vocation à réaliser de tels comparatifs.

Néanmoins, l'ambition et les fonctionnalités de l'écosystème Data Space permettent de lister quelques caractéristiques différenciant la plateforme de ses concurrentes, attestées via un entretien avec un de ses représentants :
- Data Space va proposer la plus large collection de données Sentinel, avec une couverture spatiale et temporelle totale pour la plupart des produits
- Data Space aura aussi les données les plus récentes, et servira d'ailleurs à alimenter ensuite les autres plateformes
- Data Space intègre Sentinel Hub, l'offre de SciHub et la quasi totalité de l'offre des DIAS

Alors que l'ESA misait il y a quelques années sur une offre basée sur plusieurs plateformes, telles que SciHub, les DIAS, l'ambition est donc désormais de proposer un point d'accès unique pour les données Copernicus avec Data Space.

Les parties ci-dessous abordent les différences avec les autres grandes plateformes existantes:

**[Open Access Hub / SciHub](https://scihub.copernicus.eu/)**

Jusqu'à présent le portail de référence pour accéder aux données Copernicus, SciHub va disparaître fin 2023 pour que Data Space soit le seul point d'accès européen.

Data Space propose en outre des services de calculs, ce que SciHub n'a jamais proposé.


**[Sentinel Hub](https://www.sentinel-hub.com/)**

Sentinel Hub est désormais intégré à l'écosystème Data Space, bien qu'il s'agisse d'une brique commerciale opérée par Sinergise et VITO.

Sentinel Hub va donc jouer un rôle central dans Data Space, et gagne ainsi en importance et longévité.

Cela signifie au passage que les architectes logiciels ayant conçu des solutions basées sur Sentinel Hub vont non seulement pouvoir continuer de les exploiter, mais vont aussi bénéficier de toute l'offre Data Space.


**[Amazon Web Services (AWS)](https://aws.amazon.com/fr/earth/)**

L'offre Amazon est exclusivement centrée sur la mise à disposition des services du cloud AWS pour répondre à des besoins propres au domaine du traitement de données spatiales.

Développés et administrés par des organismes/sociétés très divers, ces services ont juste pour point commun d'être hébergés chez AWS.

Il est donc difficile de considérer cette offre comme stable et pérenne, puisqu'il n'existe aucun pilotage stratégique clair et affirmé de la part d'Amazon. 

Chaque société proposant un service, que ce soit un catalogue de données ou des mécanismes de calcul, est libre de fixer ses conditions d'accès et prix.

A ce jour, cette offre comprend surtout :
- des jeux de données hébergés sur Amazon S3, administrés par des sociétés telles que Sinergise, avec des couvertures spatiales/temporelles globalement moins bonnes que Data Space
- quelques APIs de catalogage, comme une API STAC (associée à un espace de stockage S3) pour rechercher des données Sentinel-2

La seule véritable action d'Amazon a été de regrouper ces offres sur une seule et même [Market Place](https://aws.amazon.com/fr/earth/) de façon à donner de la visibilité aux différents services et applications tirant parti d'AWS dans le domaine du spatial.

En résumé, cette offre déjà ancienne a pour intérêt de s'appuyer sur le cloud le plus populaire, pour lequel de très nombreux architectes et développeurs logiciel sont formés.
Néanmoins, elle se compose d'une myriade de services sans recherche de cohérence globale et de pérennité, à l'inverse de Data Space.
A l'avenir, pour l'accès aux données Copernicus, Data Space devrait devenir la solution de référence. Il est néanmoins possible que certaines sociétés développent des répliques d'une partie du catalogue de Data Space sur des datacenters AWS localisés sur chaque continent, de façon à permettre un accès performant aux données à des organismes situés hors d'Europe. 


**[Google Earth Engine](https://earthengine.google.com/)**

La particularité de l'environnement Google Earth Engine (GEE) est de fournir une expérience utilisateur/développeur complètement intégrée dans la stack technique Google pour visualiser et traiter des données spatiales.

Basée sur assez peu de standards (hormis S3 pour l'hébergement/téléchargement de données), l'objectif de la plateforme est de proposer un environnement facile à utiliser (via des outils propriétaires Google) pour rechercher des données, les visualiser et développer des traitements.

Plus qu'un espace de stockage, GEE est donc plutôt un environnement de conception de traitements, très efficace et simple d'utilisation.

Dans l'esprit des environnements propriétaires intégrés typiques de Microsoft et ESRI, le gain en simplicité d'utilisation s'accompagne de coûts généralement élevés et de possibilités d'optimisation/extension très réduites.

Au sein de l’écosystème Data Space, l'outil le plus proche est sans doute openEO, avec son éditeur web et sa logique de scripts de traitements. Parmi les différences notables, il y a le support du langage R côté openEO (seulement Javascript et Python côté GEE), mais cette comparaison n'est pas suffisante car elle n'aborde pas dans le détail les fonctionnalités/technologies disponibles pour chaque langage, ce qui est plus important que le langage lui-même.

D'autre part, openEO propose une extension GEE, de façon à appeler le moteur Google pour accéder à des données et réaliser des calculs. Son ambition semble donc supérieure à celle de GEE, dans l'esprit de bon nombre de briques Open Source architecturées pour supporter des drivers divers.

Il sera intéressant à l'avenir de comparer GEE et openEO pour comprendre plus finement les bénéfices et inconvénients de chacun.

Enfin, tout comme avec AWS, il est possible que Google capitalise à l'avenir sur des répliques d'une partie du catalogue de Data Space sur des datacenter Google Cloud localisés sur chaque continent, de façon à permettre un accès performant aux données à des organismes situés hors d'Europe.


**[Microsoft Planetary Computer](https://planetarycomputer.microsoft.com/)**

Basée sur différents standards et briques répandues (S3, STAC, JupyterHub), Microsoft a conçu cette plateforme, hébergée sur son cloud Azure, pour proposer une expérience intégrée permettant de rechercher des données, les visualiser, les traiter et les intégrer à des applications.

Là où Google a misé sur une expérience encore plus intégrée et simple d'utilisation, via des technologies propriéraires, Microsoft a plutôt choisi d'exploiter des technologies existantes (open source ou propriétaires) pour fournir une expérience similaire.

Cette offre est donc assez proche dans l'esprit de celle de Data Space, qui se compose aussi de briques open source et propriétaires pour proposer l'ensemble des fonctionnalités de catalogage / visualisation / calcul.

Avec un catalogue administré en partie par Microsoft, une stratégie claire et ambitieuse (contrairement à Amazon) et l'emploi de standards (contrairement à Google), Microsoft Planetary Computer est un bon challenger pour Data Space et dispose de datacenters répartis dans le monde entier pour proposer ses services (contrairement à Data Space).

Néanmoins, pour la fourniture de données Copernicus, Microsoft s'est appuyé, comme Amazon, sur Sinergise pour l'administration des jeux de données, qui offrent une couverture spatiale et temporelle moins bonne que Data Space.

**Les DIAS**

La véritable différence entre Data Space et les DIAS réside dans :
- la couverture spatiale/temporelle des données Copernicus proposées, quasi totale pour Data Space alors que les DIAS étaient contraintes de proposer une petite partie du catalogue pour des raisons budgétaires
- la mise à disposition d'APIs de calculs, ce qu'aucune plateforme DIAS ne proposait et qui était très peu clair dans le cahier des charges de l'ESA
- un financement par la Commission Européenne / ESA garanti jusqu'en 2028 (6 ans à compter du lancement du projet), avec une extension possible jusqu'en 2032 (10 ans)

Autrement dit, Data Space comble les trois lacunes des DIAS :
- le financement court termiste des DIAS a freiné bon nombre d'utilisateurs potentiels, peu enclins à investir de leurs temps et capitaliser sur une solution dont la pérennité semblait hypothétique
- la couverture spatiale et temporelle des données Copernicus, quasi totale avec Data Space, rend possible le développement de nombreux services basés sur de fortes profondeurs temporelles et des données hors d'Europe (voir l'[étude de quelques cas d'usage](#​66​-etude-de-cas-dusage)).
- proposer des APIs de calculs, et un environnement proche de Google Earth Engine ou Microsoft Planetary Computer, permet à des développeurs de se focaliser sur leur métier (concevoir des algorithmes et modèles) plutôt que devoir mettre au point, installer et administrer une infrastructure de calcul comme c'était le cas avec les DIAS

Les DIAS sont donc bel et bien morts (sauf deux d'entre eux, décrits ci-dessous), moins par leur faute que par un manque de vision de la Commission Européenne et l'ESA quant à la manière de développer un écosystème souverain de services d'accès et traitement de données spatiales.

Néanmoins, deux DIAS vont continuer d'exister :
- **WEKEO** : considéré dès le départ comme un DIAS particulier, avec un cahier des charges allégé par rapport à d'autres DIAS, Wekeo se focalise sur les données environnementales, avec une clientèle identifiée dès le début sur ce sujet (monde académique en premier lieu), et va poursuivre ses activités de façon indépendante
- **CREODIAS** : désormais partie prenante de Data Space, en tant que branche commerciale fournissant des services cloud, il ne reste véritablement de cet ancien DIAS que le Cloud Provider CloudFerro (leader du DIAS CREODIAS), auquel est désormais associé l'autre Cloud Provider Open Telekom Cloud (T-Systems). 



### ​6.6.​ Etude de cas d'usage

**Contexte**

En 2022, dans le cadre d'action DIAS Priming, financée par le CNES, SOMEWARE a été amené à conseiller quatre entreprises, sélectionnées par le CNES, souhaitant implanter leurs chaînes de traitements sur les DIAS.

A partir des besoins propres à chaque entreprise, Someware a cherché à identifier les facteurs clé déterminant le choix d’une plateforme en fonction d’un cas d’usage : nature des données nécessaires, performances de téléchargement / calcul attendues, caractéristiques de l’environnement cloud….
Ensuite, Someware a réalisé une étude comparative des plateformes DIAS au regard de ces facteurs, en cherchant à obtenir auprès de chaque plateforme les informations nécessaires à l’identification de la ou des plateformes les plus adaptée(s) à chaque cas d’usage.

La présente étude propose ci-dessous une réflexion sur l'adéquation de la plateforme Data Space aux cas d'usage de ces quatre entreprises. 

#### **Société A / Traitement à très grande échelle**

**Contexte/besoins** :
- La société A met au point des produits satellitaires à échelle mondiale, et se spécialise sur le traitement des couleurs.
- Elle développe une chaîne de traitement combinant les outils Maja/Wasp ainsi que Sen2core pour produire un niveau L3A mondial. En bout de chaîne, certains traitements colorimétriques nécessitent l'intervention d'un opérateur.
- Les difficultés qu'elle rencontre aujourd'hui sont liés aux temps de téléchargement / traitement longs des données pour une telle échelle de traitement.
- D'autre part, elle ne maîtrise pas le déploiement de calculs dans le cloud.

**Solutions possibles avec Data Space**:
- Les volumes de données téléchargés par la société A, ainsi que la bande passante attendue, dépassent très certainement les quotas de la plateforme. Il est sans doute souhaitable pour cette société de contacter le service commercial afin de connaître les prix pour son usage.
- Elle peut aussi se pencher sur le déploiement cloud de ses calculs, de façon à bénéficier de quotas illimités de téléchargement quand les calculs sont déployés sur une des VMs des deux clouds proposés par Data Space. L'essentiel des calculs pourraient ainsi être réalisés sur des VMs, puis les données résultantes pourraient être rapatriées en local pour réaliser les derniers traitements colorimétriques.

#### **Société B / Accès aux données de (et depuis) la zone Pacifique Sud**

**Contexte/besoins** :
- La société B met au point des outils d’aide à la décision basés sur la télédétection à destination des territoires insulaires du Pacifique Sud (où la société est basée).
- Elle a pour projet de mettre au point et diffuser des données L2A (outils Maja / Sen2core) à l’échelle du Pacifique Sud.
- D'autre part, elle souhaite accéder aux données Sentinel-1 dès leur mise en ligne pour un projet de surveillance maritime.
- De façon générale, son besoin est de pouvoir accéder à des produits bruts récents ou données traitées à l’échelle du Pacifique Sud, avec de bons temps de téléchargement.
- Elle dispose en revanche de compétences en déploiement de calculs dans un data center local.

**Solutions possibles avec Data Space**:
- Pour cette société basée en Océanie, Data Space ne propose pas de nouveautés par rapport aux DIAS. 
- Les données de Data Space sont hébergées en Allemagne et Pologne, sans réplication sur d'autres clouds répartis dans le monde, permettant un temps d'accès très performants pour des sociétés basées hors d'Europe.
- Cette société devrait peut-être étudier la location d'une VM dans un des clouds de Data Space, pour télécharger des données sans limite, les traiter puis rapatrier seulement sur son DataCenter les résultats (encodés de la façon la plus efficace possible pour réduire les temps de téléchargement).

#### **Société C / Optimisation de temps de téléchargement / traitement**

**Contexte/besoins** :
- La société C met au point un service d’analyse géographique, essentiellement en Afrique, à partir d’imagerie Sentinel-1 et Sentinel-2.
- Elle maîtrise l'automatisation ainsi que la conception d'algorithmes de traitement, mais pas le déploiement de calculs dans le cloud.
- Elle souhaite globalement optimiser ses temps de téléchargement / traitement et, si possible, pouvoir déclencher ses traitements dès qu'une donnée est disponible (notion de "triggers").
- Elle cherche aussi à accéder à des données sur de vastes zones, avec une forte profondeur temporelle.

**Solutions possibles avec Data Space**:
- La plateforme propose l'ensemble du catalogue Sentinel-1 et Sentinel-2 en accès immédiat (IAD), autrement dit accéder à des données sur l'Afrique, avec une forte profondeur temporelle, est aisé.
- Pour disposer de temps de téléchargement très rapides, sans limite de taille, la solution sans doute la moins coûteuse est de louer une VM dans le cloud d'une des plateformes car, dans ce cas, les quotas de téléchargement sont illimités. 
- La plateforme ne propose pas de mécanisme de type "trigger" mais prévoit d'en proposer à l'avenir. Elle permet néanmoins, via l'API openEO, de développer des traitements qui se déclenchent dès la parution d'un nouveau jeu de données. Il s'agit sans doute d'une bonne réponse aux besoins de la société C.

#### **Société D / Chargement de données pour une interface de WebMapping**

**Contexte/besoins** :
- La société D développe des solutions de cartographie multi-source, basée notamment sur des données spatiales.
- Elle exploite notamment des données Sentinel-1, Sentinel-2 et Pléiades Neo en Europe et Afrique.
- Son usage le plus courant est le chargement de données en fonds de plan (tuiles / WMTS) dans une interface de WebMapping.

**Solutions possibles avec Data Space**:
- Pour cette société, Data Space propose les APIs Sentinel Hub (brique pré-existante, faisant néanmoins partie de l'écosystème Data Space). Une des APIs Sentinel Hub permet d'accéder à des données selon le protocole WMTS (utilisé par le Browser de Data Space)


## 7.​ Conclusion

Copernicus Data Space s'annonce comme un challenger de poids dans l'écosystème des plateformes de données spatiales.

Avec une ambition, et surtout des moyens, supérieurs à ceux octroyés aux DIAS, l'ESA fait le pari d'une plateforme de référence pour les données Copernicus, plutôt que plusieurs petites plateformes en compétition.

Avec un catalogue et des fonctionnalités de calculs rivalisant aisément avec les solutions concurrentes (Google, Amazon, Microsoft), la plateforme devrait vraisemblablement attirer de nombreux usagers.

Parmi les freins possibles à son succès, il y a néanmoins : 
- des quotas gratuits, et coûts d'utilisation au-delà, encore assez difficiles à évaluer, fortement dépendants de chaque cas d'usage. Un travail de documentation et de pédagogie doit être réalisé par l'ESA et le consortium.
- des temps d'accès aux données potentiellement problématiques hors d'Europe, rendant la plateforme moyennement efficiente pour des usagers d'autres continents (quand le temps de téléchargement est critique pour leurs cas d'usage)

Ces inconvénients seront certainement corrigés à moyen terme.

Le véritable questionnement qui subsiste concerne le modèle économique de la plateforme, entremêlement de services publics (essentiellement gratuits) et de services privés (payants), et ses orientations futures.

En construisant Copernicus Data Space à partir de nombreuses briques propriétaires, même si éprouvées et de qualité, le consortium a clairement cherché à être incontournable à l'avenir pour opérer et maintenir la plateforme.

En particulier, au sein du consortium, la société Sinergise (acquise par Planet durant l'été 2023) a su se positionner derrière pratiquement toutes les plateformes de données spatiales, et peut donc influencer de nombreuses évolutions en fonction de ses intérêts propres.

A quel point l'ESA souhaitera-t-elle, ou pourra-t-elle, à l'avenir imposer certaines orientations stratégiques au consortium ? Comment pourra-t-elle le mettre en compétition dans le cadre de futurs marchés publics ?

