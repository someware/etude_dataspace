# Pour exécuter les scripts de test

## Installer l'environnement Python

Sous Linux:
```
python -m venv venv
source venv/bin/activate
pip install requests
```

## Exécuter les scripts

Sous Linux, renseigner votre login/password:
```
export DSUSERNAME=XXXXXXXXXXXXXx
export DSPASSWORD=XXXXXXXXXXXXX
```

Puis lancer un script Python:
```
source venv/bin/activate
python download_product.py
```
