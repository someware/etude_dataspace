from pystac_client import Client

catalog = Client.open("https://catalogue.dataspace.copernicus.eu/stac/")
print(f"{catalog.title}")

collection = catalog.get_collection("SENTINEL-2")
print(f"{collection.title}")

results = catalog.search(
    collections=[collection],
    max_items=1,
    # bbox=[-73.21, 43.99, -73.12, 44.05],
    datetime=['2023-01-01T00:00:00Z', '2023-06-01T00:00:00Z'],
)
for item in results.items():
    print(item)