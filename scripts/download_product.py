import os
import json
import requests

def get_access(username: str, password: str) -> str:
    data = {
        "client_id": "cdse-public",
        "username": username,
        "password": password,
        "grant_type": "password",
        }
    try:
        r = requests.post("https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token",
        data=data,
        )
        r.raise_for_status()
    except Exception as e:
        raise Exception(
            f"Access token creation failed. Reponse from the server was: {r.json()}"
            )
    return r.json()["access_token"]
        

if __name__ == '__main__':

    username = os.environ['DSUSERNAME']
    password = os.environ['DSPASSWORD']

    # get and print access token
    access_token = get_access(username, password)
    print(access_token)

    # download a product
    session = requests.Session()
    session.headers.update({'Authorization': f'Bearer {access_token}'})
    url = f"https://catalogue.dataspace.copernicus.eu/odata/v1/Products(db0c8ef3-8ec0-5185-a537-812dad3c58f8)/$value"
    response = session.get(url, allow_redirects=False)
    while response.status_code in (301, 302, 303, 307):
        url = response.headers['Location']
        response = session.get(url, allow_redirects=False)

    file = session.get(url, verify=False, allow_redirects=True)

    with open(f"product.zip", 'wb') as p:
        p.write(file.content)
